
import React, { Component, PureComponent } from 'react'
import PlaylistDetails from '../components/PlaylistDetails'
import PlaylistList from '../components/PlaylistList'
import PlaylistEditForm from '../components/PlaylistEditForm'
import { Playlist } from '../../common/models/Playlist'

enum Modes {
  show = 'show',
  edit = 'edit'
}

interface Props {

}
interface State {
  playlists: Playlist[],
  selected: Playlist | null
  mode: Modes
}

class PlaylistsView extends PureComponent<Props, State> {
  state: State = {
    selected: null,
    mode: Modes.show,
    playlists: [{
      id: 123,
      name: 'Test 123',
      favorite: true,
      color: '#ff00ff'
    }, {
      id: 234,
      name: 'Test 345',
      favorite: false,
      color: '#00ffff'
    }]
  }

  select = (selected: Playlist | null) => {
    this.setState({
      selected
    })
  }

  edit = () => {
    this.setState({ mode: Modes.edit })
  }

  cancel = () => {
    this.setState({ mode: Modes.show })
  }

  save = (draft: Playlist) => {
    console.log(draft)
  }



  render() {
    return (
      <div>
        <h2>Playlists View</h2>

        <div className="row">
          <div className="col">
            <PlaylistList
              selected={this.state.selected}
              playlists={this.state.playlists}
              onSelect={this.select} />


          </div>
          <div className="col">

            {this.state.selected ?
              <>
                {this.state.mode === Modes.show &&

                  <PlaylistDetails
                    playlist={this.state.selected}
                    onEdit={this.edit} />}

                {this.state.mode === Modes.edit &&
                  <PlaylistEditForm playlist={this.state.selected}
                    onCancel={this.cancel} onSave={this.save} />
                }
              </>
              : <p>Please select playlist</p>}
          </div>
        </div>
      </div>
    )
  }
}


export default PlaylistsView