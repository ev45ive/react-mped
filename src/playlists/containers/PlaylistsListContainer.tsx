// tsrcredux 
import React, { Component } from 'react'
import { connect, MapStateToPropsParam, MapDispatchToPropsParam } from 'react-redux' // npm install @types/react-redux
import PlaylistList from '../components/PlaylistList'
import { Playlist } from '../../common/models/Playlist'
import { selectors, actionCreators } from '../../reducers/playlistsReducer'

type TStateProps = {
  playlists: Playlist[],
  selected: Playlist | null,
}

type TOwnProps = {
  // selected: Playlist | null
}

type State = any

type TDispatchProps = {
  onSelect: (selected: Playlist | null) => void
}

const mapStateToProps: MapStateToPropsParam<TStateProps, TOwnProps, State> = (state, ownProps) => ({
  playlists: selectors.playlists(state),
  selected: selectors.selectedPlaylist(state)
})
  
const mapDispatchToProps: MapDispatchToPropsParam<TDispatchProps, TOwnProps> = (dispatch, ownProps) => ({
  onSelect(selected: Playlist | null) {

    dispatch(actionCreators.select(selected?.id))
  }
})

const withPlaylists = connect(mapStateToProps, mapDispatchToProps)

export default withPlaylists(PlaylistList)

// Multiple States with same View
// export default connect(mapFavoriteListState)(PlaylistList)
// export default connect(mapMyListState)(PlaylistList)
// export default connect(mapRecentListState)(PlaylistList)

// Same State displayed in many views
// export default withPlaylists(PlaylistTable)
// export default withPlaylists(PlaylistChart)
