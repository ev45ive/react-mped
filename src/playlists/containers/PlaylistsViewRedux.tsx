
import React, { Component, PureComponent, useMemo, useEffect } from 'react'
import PlaylistDetails from '../components/PlaylistDetails'
import PlaylistList from '../components/PlaylistList'
import PlaylistEditForm from '../components/PlaylistEditForm'
import { Playlist } from '../../common/models/Playlist'
import { store } from '../../store'
import { selectors, actionCreators } from '../../reducers/playlistsReducer'
import PlaylistsListContainer from './PlaylistsListContainer'
import { SelectedPlaylistDetails, SelectedPlaylistEditForm } from './SelectedPlaylistContainers'
import { useStore, useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'



const PlaylistsViewRedux = () => {
  // const [state, dispatch] = useReducer(reducer, initialState, init)
  // // const [state, setstate] = useState(initialState)
  // const {
  //   playlists: {
  //     mode, selectedId
  //   }
  // } = useStore<any>().getState()
  // const dispatch = useDispatch()
  // mojKreatorAkcji(parametry)(dispatch)
  const selected = useSelector(selectors.selectedPlaylist)
  const { mode } = useSelector(selectors.featureSelect)
  const { playlist_id } = useParams()
  const dispatch = useDispatch()


  useEffect(() => {
    dispatch(actionCreators.select(playlist_id))
  }, [playlist_id])

  // React.memo( COmponent )
  return useMemo(() => (
    <div>
      <h2>Playlists View</h2>


      <div className="row">
        <div className="col">
          <PlaylistsListContainer />
        </div>
        <div className="col">

          {selected && mode == 'show' && <SelectedPlaylistDetails 
                                          id={selected.id} />}
          {selected && mode == 'edit' && <SelectedPlaylistEditForm id={selected.id} />}
          {!selected && <p>Please select playlist</p>}
        </div>
      </div>
    </div>
  ), [selected, mode])
}


export default PlaylistsViewRedux