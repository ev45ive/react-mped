
import React, { Component } from 'react'
import { connect } from 'react-redux'
import PlaylistDetails from '../components/PlaylistDetails'
import PlaylistEditForm from '../components/PlaylistEditForm'
import { selectors, actionCreators } from '../../reducers/playlistsReducer'
import { bindActionCreators, Dispatch } from 'redux'

const mapStateToProps = (state: any, ownProps: any) => ({
  // playlist: selectors.selectedPlaylist(state)!
  playlist: selectors.selectPlaylistById(state, { 
    id: ownProps.id 
  })
})

// const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
//   onEdit: actionCreators.edit,
//   onCancel: actionCreators.show,
//   onSave: actionCreators.save,
// }, dispatch)

const mapDispatchToProps = {
  onEdit: actionCreators.edit,
  onCancel: actionCreators.show,
  onSave: actionCreators.save,
}

const withSelectedPlaylist = connect(mapStateToProps, mapDispatchToProps)

export const SelectedPlaylistDetails = withSelectedPlaylist(PlaylistDetails)
export const SelectedPlaylistEditForm = withSelectedPlaylist(PlaylistEditForm)