import React, { Component, PureComponent } from 'react'
import { Playlist } from "../../common/models/Playlist"

interface Props {
  selected: Playlist | null
  playlists: Playlist[],
  onSelect: (selected: Playlist | null) => void
}


export default class PlaylistList extends PureComponent<Props> {

  select(selected: Playlist) {
    this.props.onSelect(selected)
  }

  render() {  
    return (
      <div>
        <div className="list-group">
          {
            this.props.playlists.map((playlist, i) => <div
              onClick={(event) => this.select(playlist)}
              className={`list-group-item ${
                this.props.selected === playlist ? 'active' : ''
                }`}
              key={playlist.id}>

              <span>{i + 1}. {playlist.name}</span>
            </div>)
          }
        </div>
      </div >
    )
  }
}
