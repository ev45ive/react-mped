import React, { Component, PureComponent } from 'react'
import { Playlist } from '../../common/models/Playlist'

(window as any).React = React

interface Props {
  playlist: Playlist
  onCancel?: () => void
  onSave: (draft: Playlist) => void
}

interface State {
  playlist: Playlist
}


export default class PlaylistEditForm extends PureComponent<Props, State> {

  static defaultProps = {
    onCancel(){}
  }

  constructor(props: Props) {
    super(props)
    this.state = { playlist: props.playlist }
  }

  // shouldComponentUpdate(nextProps: Props, nextState: State){
  //   // console.log('shouldComponentUpdate',nextState)
  //   return this.state.playlist !== nextState.playlist || nextProps.playlist !== this.props.playlist
  // }

  componentWillUnmount() { }

  getSnapshotBeforeUpdate(prevProps: Props, prevState: State) {
    return {
      scrollPosTop: 200
    }
  }

  componentDidUpdate(prevProps: Props, prevState: State, snapshot: any) {
    console.log(snapshot)
  };

  // parent->onSetState && this->onSetState
  // this.setState( stateTransformingFunction )
  static getDerivedStateFromProps(nextProps: Readonly<Props>, prevState: State) {
    // console.log('getDerivedStateFromProps',prevState)
    return ({
      playlist: prevState.playlist.id === nextProps.playlist.id ? prevState.playlist : nextProps.playlist
    })
  }


  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const target = event.currentTarget
    const type = target.type
    const fieldName = target.name;
    // this.state.playlist.name = target.value
    // this.setState({})
    
    this.setState(prevState => ({
      playlist: {
        ...prevState.playlist,
        [fieldName]: type === 'checkbox' ? target.checked : target.value
      }
    }))
  }

  
  componentDidMount() {
    this.nameInputRef.current?.focus()
  }

  nameInputRef = React.createRef<HTMLInputElement>()

  render() {
    return (
      <div>
        <div className="form-group" >
          <label htmlFor="playlistFormName">Name:</label>
          <input type="text" id="playlistFormName" className="form-control" name="name"
            value={this.state.playlist.name}
            onChange={this.handleChange} ref={this.nameInputRef} />

          {170 - this.state.playlist.name.length} / 170
        </div >

        <div className="form-group">
          <label htmlFor="playlistFormFavorite">Favorite:</label>
          <input type="checkbox" id="playlistFormFavorite" checked={this.state.playlist.favorite} name="favorite"
            onChange={this.handleChange} />
        </div>

        <div className="form-group">
          <label htmlFor="playlistFormColor">Color:</label>
          <input type="color" id="playlistFormColor" value={this.state.playlist.color}
            onChange={this.handleChange} name="color" />
        </div>

        <input type="button" value="Cancel" className="btn btn-danger" onClick={this.props.onCancel} />
        <input type="button" value="Save" className="btn btn-success" onClick={() => this.props.onSave(this.state.playlist)} />
      </div>
    )
  }
}
