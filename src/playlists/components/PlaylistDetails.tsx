
import React, { Component } from 'react'
import { Playlist } from '../../common/models/Playlist'

interface Props {
  playlist: Playlist
  onEdit: () => void
}

interface State {
}

export default class PlaylistDetails extends Component<Props> {

  render() {
    return (
      <div>
        <dl data-playlist-id={this.props.playlist.id}>

          <dt>Name:</dt>
          <dd>{this.props.playlist.name}</dd>

          <dt>Favorite:</dt>
          <dd>{this.props.playlist.favorite ? 'Yes' : 'No'}</dd>

          <dt>Color:</dt>
          <dd style={{
            color: this.props.playlist.color,
            background: this.props.playlist.color
          }}>{this.props.playlist.color}</dd>
        </dl>

        <input type="button" value="Edit" className="btn btn-info"
        onClick={this.props.onEdit} />
      </div >
    )
  }
}
