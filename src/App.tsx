import React from "react";
// import logo from "./logo.svg";
// import 'bootstrap/dist/css/bootstrap.css'
import "./App.css";
import SearchView from "./search/containers/SearchView";
import SearchContextProvider from "./search/contexts/SearchContext";
import AuthContextProvider from "./common/AuthContext";
import PlaylistsView from "./playlists/containers/PlaylistsView";
import { store } from "./store";
import PlaylistsViewRedux from "./playlists/containers/PlaylistsViewRedux";
import { Provider } from "react-redux";
import UserRegistrationView from "./users/containers/UserRegistrationView";
// import { HashRouter as Router, Route, Switch, Redirect ,NavLink} from 'react-router-dom'
import { BrowserRouter as Router, Route, Switch, Redirect, NavLink } from 'react-router-dom'

function App() {
  return (
    <Provider store={store}>
      <AuthContextProvider>
        <SearchContextProvider>
          <Router>
            <Layout>

              <Switch>
                <Redirect path="/" to="/playlists" exact={true} />
                <Route path="/playlists"  exact={true} component={PlaylistsViewRedux} />
                <Route path="/playlists/:playlist_id" component={PlaylistsViewRedux} />
                <Route path="/register" component={UserRegistrationView} />
                <Route path="/search" component={SearchView} />
                {/* <Redirect path="**" to="/" /> */}
                <Route path="**" render={() => <h1>Page Not Found</h1>} />
              </Switch>


              {/* <Redirect path="/access_token" to=""/> */}

            </Layout>
          </Router>
        </SearchContextProvider>
      </AuthContextProvider>
    </Provider>
  );
}

export default App;


export const Layout: React.FC = (props) =>
  <div>
    <nav className="navbar navbar-expand navbar-dark bg-dark mb-3">
      <div className="container">
        <NavLink className="navbar-brand" exact={true} to="/">Navbar</NavLink>

        <div className="collapse navbar-collapse" >
          <ul className="navbar-nav">

            <li className="nav-item">
              <NavLink className="nav-link" to="/search">Search</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/playlists">Playlists</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/register">Register</NavLink>
            </li>

          </ul>
        </div>
      </div>
    </nav>

    <div className="container">
      <div className="row">
        <div className="col">
          {props.children}
        </div>
      </div>
    </div>
  </div>