import { Reducer, Action, ActionCreator, Dispatch } from "redux";
import { Playlist } from "../common/models/Playlist";
import { createSelector } from 'reselect'


interface State {
  entities: Record<Playlist['id'], Playlist>

  list: Array<Playlist['id']>
  favourite: Array<Playlist['id']>

  selectedId: Playlist['id'] | null,

  mode: 'edit' | 'show' | 'empty'
}

/* Key in Global State */
export const featureKey = 'playlists'

type Actions =
  | PLAYLISTS_SELECT
  | PLAYLISTS_LOAD
  | PLAYLISTS_UI_SWITCH_MODE
  | PLAYLISTS_UPDATE

const initialState: State = {
  entities: {},
  list: [],
  favourite: [],
  selectedId: null,
  mode: 'show'
}

const reducer: Reducer<State, Actions> = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.PLAYLISTS_LOAD_SUCCESS:
      return {
        ...state, list:
          action.payload.playlists.map(p => p.id),
        entities: action.payload.playlists.reduce((entities, p) => {
          return { ...entities, [p.id]: p }
        }, state.entities)
      }

    case ActionTypes.PLAYLISTS_SELECT:
      return { ...state, selectedId: action.payload.playlistId }

    case ActionTypes.PLAYLISTS_UPDATE: {
      // Dont mutate data, always return copy:
      // Immer
      // const playlists = produce(state.playlists, playlists => {
      //   const index = playlists.findIndex(p => p.id === action.payload.playlist.id)
      //   playlists[index] = action.payload.playlist
      // })
      const updated = action.payload.playlist
      return { ...state, entities: { ...state.entities, [updated.id]: updated } }
      // return { ...state, playlists: state.playlists.map(p => p.id === updated.id ? updated : p) }
    }

    case ActionTypes.PLAYLISTS_UI_SWITCH_MODE:
      return { ...state, mode: action.payload.mode }

    default:
      return state
  }
}

export default reducer

enum ActionTypes {
  PLAYLISTS_SELECT = '[Playlists] Playlist Selected',
  PLAYLISTS_LOAD_SUCCESS = '[Playlists] Loaded Playlists',
  PLAYLISTS_UPDATE = '[Playlists] Playlist Updated',
  PLAYLISTS_UI_SWITCH_MODE = '[Playlists/UI] Switched display mode',
}


interface PLAYLISTS_SELECT {
  type: ActionTypes.PLAYLISTS_SELECT,
  payload: { playlistId: Playlist['id'] }
}

interface PLAYLISTS_LOAD extends Action<ActionTypes.PLAYLISTS_LOAD_SUCCESS> {
  payload: { playlists: Playlist[] }
}

interface PLAYLISTS_UPDATE extends Action<ActionTypes.PLAYLISTS_UPDATE> {
  payload: { playlist: Playlist }
}

interface PLAYLISTS_UI_SWITCH_MODE extends Action<ActionTypes.PLAYLISTS_UI_SWITCH_MODE> {
  payload: { mode: State['mode'] }
}

/**
 * REDUCER PUBLIC API
 */

/** Action Creators */

const load: ActionCreator<PLAYLISTS_LOAD> = (playlists: Playlist[]) => ({
  type: ActionTypes.PLAYLISTS_LOAD_SUCCESS,
  payload: {
    playlists
  }
})

const update = (playlist: Playlist): PLAYLISTS_UPDATE => ({
  type: ActionTypes.PLAYLISTS_UPDATE,
  payload: {
    playlist
  }
})

const select: ActionCreator<PLAYLISTS_SELECT> = (playlistId: Playlist['id']) => ({
  type: ActionTypes.PLAYLISTS_SELECT,
  payload: {
    playlistId
  }
})

const edit: ActionCreator<PLAYLISTS_UI_SWITCH_MODE> = () => ({
  type: ActionTypes.PLAYLISTS_UI_SWITCH_MODE,
  payload: {
    mode: 'edit'
  }
})

const show: ActionCreator<PLAYLISTS_UI_SWITCH_MODE> = () => ({
  type: ActionTypes.PLAYLISTS_UI_SWITCH_MODE,
  payload: {
    mode: 'show'
  }
})


const save = (playlist: Playlist) => (dispatch: Dispatch) => {

  // dispatch(Validate)
  // dispatch(Start loading)
  // dispatch(Send to server)
  // dispatch(Fetch resuts)
  // dispatch(Show results)

  dispatch({ type: 'LOADING', payload: true })
  setTimeout(() => {
    dispatch(actionCreators.update(playlist))
    dispatch({ type: 'LOADING', payload: false })
    dispatch(actionCreators.show())
  }, 2000)

}


export const actionCreators = {
  load, update, select, edit, show, save
}

  ; (window as any).actionCreators = actionCreators;

/**
 * Selectors - PUBLIC API
 */

const featureSelect = (appState: any): State => appState[featureKey]

// const playlists = (state: any) => {
//   const localState = featureSelect(state)
//   return localState.list.map(id => localState.entities[id])
// }

const listSelector = createSelector(
  featureSelect, state => state.list
)

const entitiesSelector = createSelector(
  featureSelect, state => state.entities
)

const playlists = createSelector(
  listSelector,
  entitiesSelector,
  (list, entities) => list.map(id => entities[id])
)

const selectedPlaylist = createSelector(
  featureSelect,
  entitiesSelector,
  (state, entities) => state.selectedId ? entities[state.selectedId] : null
)

const selectPlaylistById = createSelector(
  (state: any, props: any) => props.id,
  entitiesSelector,
  (id, entities) => entities[id]
)
// selectPlaylistById(state, ownProps.id)


// const selectedPlaylist = (state: any) => {
//   const localState = featureSelect(state)
//   return localState.selectedId? localState.entities[localState.selectedId] : null
// }

// https://github.com/reduxjs/reselect


export const selectors = {
  featureSelect,
  playlists,
  selectedPlaylist,
  selectPlaylistById
}

  ; (window as any).selectors = selectors;