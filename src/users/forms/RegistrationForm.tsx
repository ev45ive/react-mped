
import React, { useState } from 'react'
import { Formik, Form, Field, ErrorMessage, FormikHelpers, FieldArray } from 'formik'

interface Props {
  onRegistered(data: FormState): void
}

interface FormState {
  username: string;
  password: string;
  options: {
    name: string
    value: string
  }[]
}
// type ValidationState = { [K in keyof FormState]?: string }
type ValidationState = Partial<Record<keyof FormState, string>> & Record<string, string>

const RegistrationForm = (props: Props) => {
  const [optionsVisible, setoptionsVisible] = useState(false)

  const onSubmit = (data: FormState, helpers: FormikHelpers<FormState>) => {
    console.log(data)
    props.onRegistered(data) // mapDispatchToProps{ ... onRegistered : actionCreators.save
  }

  const validate = (data: FormState) => {
    const errors: ValidationState = {}

    if (!data.username)
      errors.username = 'Field Required'

    if (!data.password)
      errors.password = 'Field Required'

    // errors['options.vip'] = 'Must be vip'

    return errors
  }

  const initialValues: FormState = {
    username: '',
    password: '',
    options: [
      { name: 'VIP', value: 'Yes' },
      { name: 'ABC', value: 'No' },
    ]
  }

  return (
    <div>
      <Formik
        validateOnChange
        initialValues={initialValues}
        // enableReinitialize={true} // derive state from new initialValues (ie.Props)
        validate={validate}
        onSubmit={onSubmit} >{({ handleSubmit, handleReset, values }) => <Form>


          <div className="form-group">
            <label htmlFor="">Username</label>
            <Field className="form-control" name="username"></Field>
            <ErrorMessage name="username" />
          </div>

          <div className="form-group">
            <label htmlFor="">Password</label>
            <Field className="form-control" name="password" type="password"></Field>
            <ErrorMessage name="password" />
          </div>

          <div className="p-3">
            <button onClick={() => setoptionsVisible(!optionsVisible)}>Options</button>


            {/* <Modal>*/}
            {optionsVisible && <FieldArray name="options">{({ handleInsert, remove, push }) =>
              <div>{
                values.options.map((option, index) =>
                  <div className="form-group" key={option.name}>
                    <span className="close" onClick={() => remove(index)}>&times;</span>
                    <label><Field type="text" name={`options.${index}.value`} /> {option.name} </label>
                    <ErrorMessage name={`options.${index}.value`} />
                  </div>)
              }
                <button onClick={() => push({ name: 'Option ' + (values.options.length + 1), value: '' })}>Add Option</button>
              </div>

            }
            </FieldArray>}
            {/*</Modal> */}
          </div>

          <input type="button" className="btn btn-danger" value="Cancel" onClick={handleReset} />
          <input type="submit" className="btn btn-success" value="Register" />
        </Form>}
      </Formik>

    </div >
  )
}

export default RegistrationForm
