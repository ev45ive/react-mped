

import React from 'react'
import RegistrationForm from '../forms/RegistrationForm'

interface Props { }

const UserRegistrationView = (props: Props) => {

  const onRegistered = () => { }
  
  return (
    <div>
      <h1>Registation</h1>

      <RegistrationForm onRegistered={onRegistered} />

    </div>
  )
}

export default UserRegistrationView
