import { createStore, combineReducers, applyMiddleware, Middleware } from 'redux'
import * as playlists from './reducers/playlistsReducer'


// const initialState = {
//   // users: {},
//   playlists: {},
//   search: {}
// }

// function reducer(state: any, action: any) {
//   return {
//     ...state,
//     // users: usersReducer(state.users, action),
//     // search: searchReducer(state.search, action),
//     playlists: playlistsReducer(state.playlists, action),
//   }
// }

// https://redux.js.org/recipes/structuring-reducers/reusing-reducer-logic

const reducer = combineReducers({
  // users: usersReducer,
  // search: combineReducers({
  //   albums: albumsSearchReducer,
  //   artists: artistsSearchReducer,
  // }),
  // playlists: combineReducers({
  //   deeper: createPlaylistsReducer('TopPlaylists'),
  //   deeper2: createPlaylistsReducer('MyPlaylists'),
  // })
  [playlists.featureKey]: playlists.default,
})


// A.next = B
// B.next = C
// C.next = dispatch
// A.dispatch(msg)
const logger: Middleware = (store) =>
  next =>
    action => {
      console.log('BEFORE::', store.getState())
      console.log('ACTION::', action)
      next(action)
      console.log('AFTER::', store.getState())
    }

const thunk: Middleware = store => next => action => {
  if('function' === typeof action){
    action(next)
  }else{
    next(action)
  }
}

export const store = createStore(reducer, applyMiddleware(logger, thunk) /* , initialState */);


(window as any).store = store


store.dispatch(playlists.actionCreators.load([{
  id: 123,
  name: 'Test 123',
  favorite: true,
  color: '#ff00ff'
}, {
  id: 234,
  name: 'Test 345',
  favorite: false,
  color: '#00ffff'
}]))