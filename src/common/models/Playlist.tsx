export interface Entity {
  id: number // | string
  name: string;
}

export interface Track extends Entity {
  atype: 'track'
  duration: number
}

export interface IHaveColor {
  color: string;
}

export interface Playlist extends Entity, IHaveColor {
  // atype: 'playlist'
  favorite: boolean;
  // tracks:Array<Track>
  tracks?: Track[]
}



export interface Playlist {
  id: number
  name: string
  favorite: boolean
  /**
   * HEX value
   */
  color: string
}



// const p :Playlist = {}
// p.tracks is optional

// p.tracks? p.tracks : []
// p.tracks ?? [] // default value

// p.tracks && p.tracks.length
// p.tracks?.length // save traversal
// type Entity = Playlist | Track;

// const e: Entity = {}

// switch (e.atype) {
//   case 'playlist': {
//     e.tracks
//   }
//     break;
//   case 'track': {
//     e.duration
//   }
// }


// // # TypeGuards
// let p:Playlist = {}
// p.id  // number | string
// if('number' == typeof p.id){
//   p.id.toExponential() // number
// }else{
//   p.id.trim() // string

// }





// interface Point { x: number; y: number }
// interface Vector { x: number; y: number, length: number }

// let p: Point = { x: 1, y: 2 };
// let v: Vector = { x: 1, y: 2, length: 3 };

// p = v;
// v = p;



// apply<ACommand>(cmd)
// apply<BCommand>(cmd)
// apply<CCommand>(cmd)
// apply<any>(cmd){
//   if(cmd instanceof A){}
//   switch(){

//   }
// }