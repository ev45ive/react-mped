import React, { useState, FC, useEffect } from "react";

export const AuthContext = React.createContext({
  user: null,
  isLoggedIn: false,
  token: ''
})


const AuthContextProvider: FC = (props) => {

  const [token, setToken] = useState('')

  function login() {

    // placki@placki.com 
    // ******

    const url = 'https://accounts.spotify.com/authorize?' + new URLSearchParams({
      client_id: '70599ee5812a4a16abd861625a38f5a6',
      response_type: 'token',
      redirect_uri: 'http://localhost:3000/',
      show_dialog: 'true'
    })
    window.location.replace(url)
  }

  useEffect(() => {
  
    let access_token = new URLSearchParams(window.location.hash)//
      .get('#access_token')

    let storage_token = sessionStorage.getItem('token')

    if (!access_token && storage_token) {
      access_token = JSON.parse(storage_token)
    }

    if (access_token == null) {
      login()
      return
    }

    sessionStorage.setItem('token', JSON.stringify(access_token))
    setToken(access_token)
  }, [])

  return (
    <AuthContext.Provider value={{
      token,
      user: null,
      isLoggedIn: !!token
    }}>
      {props.children}
    </AuthContext.Provider>
  )
}

export default AuthContextProvider
