import React, { FC, useState, Reducer, useReducer, useContext } from "react";
import { Album, SearchResponse } from "../../common/models/Album";
import axios from 'axios'
import { AuthContext } from "../../common/AuthContext";

// axios.interceptors.response.use(
//   success => success,
//   rejected => {
//     if(rejected.status == 401){
//       auth.refreshToken ? auth.LoggedOut()
//     }
//   }
// ))

export const SearchContext = React.createContext<
  {
    results: Album[];
    loading: boolean;
    error: Error | null,
    search: (query: string) => void;
  }>({
    loading: false,
    results: [],
    error: null,
    search() {
      throw 'Missing SearchContext Provider'
    }
  })



type State = {
  results: Album[],
  loading: boolean,
  error: Error | null,
  query: string
}

type Actions =
  | SEARCH_START
  | SEARCH_SUCCESS
  | SEARCH_FAILED

const initialState: State = {
  results: [],
  error: null,
  loading: false,
  query: ''
}

export const reducer: Reducer<State, Actions> = (state, action): State => {
  switch (action.type) {
    case 'SEARCH_START': return {
      ...state,
      results: [],
      loading: true,
      query: action.payload.query,
      error: null
    }
    case 'SEARCH_SUCCESS': return { ...state, results: action.payload.results, loading: false }
    case 'SEARCH_FAILED': return { ...state, loading: false, error: action.payload.error }
    default: return state
  }
}

const actions = {
  /**
   * Starts search
   */
  searchStart: (query: string): SEARCH_START => ({ type: 'SEARCH_START', payload: { query } }),
  searchSuccess(results: Album[]): SEARCH_SUCCESS {
    return { type: 'SEARCH_SUCCESS', payload: { results } }
  },
  searchFailed(error: Error): SEARCH_FAILED {
    return { type: 'SEARCH_FAILED', payload: { error } }
  },
}

// https://github.com/redux-utilities/redux-promise middleware
// dispatch(
//   axios.get('..')
//   .then(resp => actions.searchSuccess(resp.data)
//   .catch(err => actions.searchFailed(err)))
// )


const SearchContextProvider: FC = (props) => {
  const { token } = useContext(AuthContext)
  const [state, dispatch] = useReducer(reducer, initialState)

  async function search(query: string) {
    dispatch(actions.searchStart(query))

    axios.defaults.baseURL = 'https://api.spotify.com/v1/'

    try {
      const response = await axios.get<SearchResponse>('search', {
        params: {
          type: 'album',
          q: query
        },
        headers: {
          Authorization: 'Bearer ' + token
        },
      })
      dispatch(actions.searchSuccess(response.data.albums.items));
    } catch (error) {
      dispatch(actions.searchFailed(error))
    }

  }

  const { results, loading, error } = state
  return (
    <SearchContext.Provider value={{ results, loading, search, error }}>
      {props.children}
    </SearchContext.Provider>
  )
}

export default SearchContextProvider

interface Action<Type> {
  type: Type
}

interface SEARCH_START extends Action<'SEARCH_START'> {
  payload: {
    query: string;
  };
}

interface SEARCH_SUCCESS {
  type: 'SEARCH_SUCCESS';
  payload: {
    results: Album[];
  };
}

interface SEARCH_FAILED {
  type: 'SEARCH_FAILED';
  payload: {
    error: Error;
  };
}
