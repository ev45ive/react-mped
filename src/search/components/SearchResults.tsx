import React from 'react'
import AlbumCard from './AlbumCard'

import styles from './SearchResults.module.css'
import { Album } from '../../common/models/Album'

// console.log(styles)

// https://getbootstrap.com/docs/4.4/components/card/#card-groups

interface Props {
  results: Album[]
}

const SearchResults = React.memo((props: Props) => {
  return (
    <div>
      Search results
      <div className={`card-group ${styles.columns4}`}>
        {
          props.results.map(
            result =>
              <AlbumCard
                key={result.id}
                className={styles.card} album={result} />
          )
        }
      </div>
    </div>
  )
})

export default SearchResults
