import React, { useState, useRef, useEffect, useLayoutEffect } from 'react'

// https://getbootstrap.com/docs/4.4/components/input-group/#button-addons

interface Props extends React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
  onSearch(query: string): void
  query: string
}

const SearchForm = React.forwardRef<HTMLInputElement, Props>(
  ({ query: queryFromParent, onSearch, ...InputProps }: Props, forwaredRef) => {
    /*  */
    const [type, setType] = useState('album')
    const [query, setQuery] = useState(queryFromParent)
    const isFirst = useRef(true)

    // getDerrivedStateFromProps (when prop changed)
    useEffect(() => {
      setQuery(queryFromParent)
    }, [queryFromParent])

    useEffect(() => {
      if (isFirst.current) {
        isFirst.current = false
        return
      }

      const handle = setTimeout(() => {
        onSearch(query)
      }, 1000)

      return () => clearTimeout(handle)
    }, [query])


    return (
      <div>
        <div className="input-group mb-3">

          <input type="text" {...InputProps} ref={forwaredRef}
            value={query} onChange={e => setQuery(e.target.value)} />

          {/* <select className="form-control" value={type} onChange={e => setType(e.target.value)} style={{width:'20px'}}>
          <option value="album">Album</option>
          <option value="artist">Artist</option>
        </select> */}

          {/* <div className="input-group-append">
          <button className="btn btn-outline-secondary" onClick={() => props.onSearch && props.onSearch(query)}>Search</button>
        </div> */}
        </div>
      </div>
    )
  })

SearchForm.defaultProps = {
  onSearch() { },
  className: "form-control",
  placeholder: "Search"
}

export default SearchForm


export const useStateDebounce = (onEffect: (value: string) => void, timeout = 1000) => {
  const [query, setValue] = useState(() => '')
  const isFirst = useRef(true)

  useEffect(() => {
    if (isFirst.current) {
      isFirst.current = false
      return
    }

    const handle = setTimeout(() => {
      onEffect!(query)
    }, timeout)

    return () => clearTimeout(handle)
  }, [query])

  return [query, setValue] as const
}