import React from 'react'
import { Album } from '../../common/models/Album'

interface Props extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  album: Album
}

const AlbumCard = ({ album, ...divProps }: Props) => {
  // const {album, ...divProps } = props;

  return (
    <div className="card" {...divProps}>

      <img src={album.images[0].url}
        className="card-img-top" />

      <div className="card-body">
        <h5 className="card-title"> {album.name}</h5>
      </div>

    </div>
  )
}

export default AlbumCard

{/* <p className="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
<p className="card-text"><small className="text-muted">Last updated 3 mins ago</small></p> */}