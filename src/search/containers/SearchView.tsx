// tsrafce
import React, { useState, useRef, useEffect, ComponentType, FC, useContext } from 'react'
import SearchForm from '../components/SearchForm'
import SearchResults from '../components/SearchResults'
import { Album } from '../../common/models/Album'
import { SearchContext } from '../contexts/SearchContext'
import { RouteComponentProps, useLocation, useParams, useHistory } from 'react-router-dom'

interface Props extends RouteComponentProps { }

const SearchView = (props: Props) => {

  const { search: queryParams } = useLocation()
  const query = new URLSearchParams(queryParams).get('q') || ''

  const { search } = useContext(SearchContext)
  useEffect(() => {
    search(query)
  }, [query])

  const { push } = useHistory()
  const onSearchChange = (query: string) => {
    push({
      pathname: '/search',
      search: new URLSearchParams({ q: query }).toString()
    })
  }

  const inputRef = useRef<HTMLInputElement>(null)
  useEffect(() => inputRef.current?.focus(), [])

  return (
    <div>
      <h2>Search</h2>
      {/* .row*2>.col */}

      <div className="row">
        <div className="col">
          <SearchForm
            query={query}
            onSearch={onSearchChange}
            placeholder="Search albums"
            ref={inputRef} />

        </div>
      </div>

      <div className="row">
        <div className="col">
          <SearchResultsCtx />
        </div>
      </div>
    </div>
  )
}

export default SearchView


export const SearchResultsCtx: FC = () => {
  const { results, loading, error } = useContext(SearchContext)

  return <>
    {loading && <p>Loading...</p>}
    {error && <p>{error.message}</p>}

    {results && <SearchResults results={results} />}
  </>
}


// export const SearchFormWithContext = withSearch(SearchForm, context => ({
//   onSearch: context.search
// }))

// type ContextProps = {
//   results: Album[];
//   loading: boolean;
//   search: (query: string) => void;
// }

// function withSearch<P extends Object>(
//   Comp: React.ComponentType<P>,
//   fn: (ctx: ContextProps) => Exclude<P, ContextProps>) {

//   return (props: any) => {

//     return <SearchContext.Consumer>{
//       (context) => <Comp {...props} {...fn(context)} />
//     }</SearchContext.Consumer>
//   }
// }
