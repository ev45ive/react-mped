npm i -g create-react-app
https://bitbucket.org/ev45ive/react-mped/src

create-react-app --template=typescript mped-react

npm i --save bootstrap

# Editor

https://marketplace.visualstudio.com/items?itemName=EQuimper.react-native-react-redux
https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets
https://marketplace.visualstudio.com/items?itemName=walter-ribeiro.full-react-snippets

https://code.visualstudio.com/docs/editor/userdefinedsnippets

# Linter

npm install eslint --save-dev
node_modules/eslint/bin/eslint.js --init

https://eslint.org/docs/user-guide/getting-started
https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint

# Formatter

npm install --save-dev eslint-config-prettier tslint-plugin-prettier prettier
https://prettier.io/docs/en/integrating-with-linters.html

# Stroybook / Components Gallery

https://storybook.js.org/docs/guides/guide-react/

# Structure

/home/
/users/
/playlists/
/containers/
/components/

/common/
/shared/
/core/
/components/

/styleguide/
/storybook/

# Emmet

https://docs.emmet.io/cheat-sheet/

#root>div[attriute=value].class
.container>.row>.col
.row>.col\*2

# OOCSS / Components

http://www.stubbornella.org/content/2010/06/25/ the-media-object-saves-hundreds-of-lines-of-code/

https://styled-components.com/
https://emotion.sh/docs/introduction

# Proces

- Makieta UI
- Jaki stan ( co wyświetlamy )
- Jakie akcje ( co zmieniamy )
- Czas / Cykl życia tego stanu
- Zasięg ( kto jeszce go potrzebuje )
- Serializacja

# Immutability vs Performance - React.Memo() & PureComponent
immerjs.github.io/immer/docs/introduction

```ts
// immerjs.github.io/immer/docs/introduction
https: const playlists = produce(state.playlists, (playlists) => {
  const index = playlists.findIndex((p) => p.id === action.payload.playlist.id);
  playlists[index] = action.payload.playlist;
});

// Dont mutate data, always return copy:
const updated = action.payload.playlist;
return {
  ...state,
  playlists: state.playlists.map((p) => (p.id === updated.id ? updated : p)),
};
```

# Middlewares
npm i redux-logger 
npm i redux-thunk

// https://en.wikipedia.org/wiki/Thunk

// https://github.com/redux-utilities/redux-promise middleware
https://redux-saga.js.org/


# Selectors + Memoization
https://github.com/reduxjs/reselect